package com.example.tp1;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp1.R;

class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView label = null;
    ImageView icon = null;
    LinearLayout line = null;

    RowHolder(View row) {
        super(row);
        this.label = (TextView) row.findViewById(R.id.name);
        this.icon = (ImageView) row.findViewById(R.id.image);
        this.line = (LinearLayout) row.findViewById(R.id.row);
        this.line.setOnClickListener(this);
    }

    void bindModel(String item, int imgId) {
        this.label.setText(item);
        this.icon.setImageResource(imgId);
    }

    @Override
    public void onClick(View view) {
        int position = this.getAdapterPosition();
        final String[] animals = AnimalList.getNameArray();
        Intent switchToAnimal = new Intent(view.getContext(), AnimalActivity.class);
        switchToAnimal.putExtra("nameAnimal", animals[position]);
        itemView.getContext().startActivity(switchToAnimal);
    }

}
