package com.example.tp1;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);
        String name = getIntent().getExtras().getString("nameAnimal");
        final Animal current = AnimalList.getAnimal(name);

        ImageView image = (ImageView) findViewById(R.id.imageViewAnimal);
        if (current.getImgFile() != null) {
            Log.i("item: ", current.getImgFile().toLowerCase());
            int resID = this.getResources().getIdentifier(current.getImgFile().toLowerCase(), "drawable", "com.example.tp1");
            Log.i("resid: ", String.valueOf(resID)); //will return 0 - im guessing this is where the problem is
            image.setImageResource(resID);
        }

        TextView fill_name = (TextView) findViewById(R.id.name);
        fill_name.setText(name);

        String life = current.getStrHightestLifespan();
        TextView fill_life = (TextView) findViewById(R.id.lifespan);
        fill_life.setText(life);

        String gest = current.getStrGestationPeriod();
        TextView fill_gest = (TextView) findViewById(R.id.gestation);
        fill_gest.setText(gest);

        String birthweight = current.getStrBirthWeight();
        TextView fill_birth = (TextView) findViewById(R.id.birthweight);
        fill_birth.setText(birthweight);

        String adultweight = current.getStrAdultWeight();
        TextView fill_adult = (TextView) findViewById(R.id.adultweight);
        fill_adult.setText(adultweight);

        final TextInputEditText fill_conser = (TextInputEditText) findViewById(R.id.conservation);
        String conservation = current.getConservationStatus();
        fill_conser.setText(conservation);

        Button save = (Button) findViewById(R.id.save_button);
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                current.setConservationStatus(fill_conser.getText().toString());
            }
        });
    }
}
