package com.example.tp1;

import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.Toast;


import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    final String[] animals = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final RecyclerView rv = (RecyclerView) findViewById(R.id.listViewAnimals);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());
    }

    public Resources resources() {
        return this.getResources();
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.cell, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            final Animal current = AnimalList.getAnimal(animals[position]);
            int resID = 0;
            if (current.getImgFile() != null) {
                Log.i("item: ", current.getImgFile().toLowerCase());
                resID = resources().getIdentifier(current.getImgFile().toLowerCase(), "drawable", "com.example.tp1");
                Log.i("resid: ", String.valueOf(resID)); //will return 0 - im guessing this is where the problem is
            }
            holder.bindModel(animals[position], resID);
        }

        @Override
        public int getItemCount() {
            return(animals.length);
        }
    }

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listview = (ListView) findViewById(R.id.listViewAnimals);
        final String[] animals = AnimalList.getNameArray();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, animals);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // Do something in response to the click
                final String item = (String) parent.getItemAtPosition(position);
                Toast.makeText(MainActivity.this, "You selected: " + item, Toast.LENGTH_LONG).show();
            }
        });

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent switchToAnimal = new Intent(MainActivity.this, AnimalActivity.class);
                switchToAnimal.putExtra("nameAnimal", animals[position]);
                startActivity(switchToAnimal);
            }
        });
    }*/
}
